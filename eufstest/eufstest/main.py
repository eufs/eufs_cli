import os
import subprocess
from subprocess import Popen, DEVNULL, PIPE
import logging
import yaml
from colorama import Fore
from eufscli import CommandExtension
import junitparser
from junitparser import JUnitXml
from junit2htmlreport import runner
from os import symlink
import datetime


class EUFSTest(CommandExtension):
    '''
    Run tests (wrapper for colcon test and colcon test-result)

    A wrapper for colcon test and colcon test-result.

    Prints a summary of runned test packages, merges all the
    junit xml files and gives a link to the generated html
    file.
    '''

    def configure(self, parser):
        parser.add_argument(
            "packages",
            nargs="*",
            default=[],
            help="Space separated list of packages. Left as blank to test all packages. ",
        )
        parser.add_argument(
            "-a",
            "--all",
            action="store_true",
            dest="all",
            help="Ignore blacklist in eufsbuild.yaml",
        )
        parser.add_argument(
            "-s",
            "--skip",
            nargs="*",
            dest="skip",
            default=[],
            help="Space separated list of packages to skip. Not Implemented. ",
        )
        parser.add_argument(
            "-c",
            "--console",
            action="store_true",
            dest="print_to_console",
            help="Print test results directly to console.",
        )
        parser.add_argument(
            "--regex",
            nargs="*",
            dest="regex",
            default=[],
            help="Regex to isolate tests.",
        )

    def main(self, args):
        logger = logging.getLogger("eufs_cli.test")
        logger.debug("Running eufs test")

        eufs_master = os.environ["EUFS_MASTER"]
        repo_info_path = os.path.join(eufs_master, ".eufscli", "repos.yaml")

        # Read yaml file with eufs repository paths
        with open(repo_info_path, "r") as f:
            repo_info = yaml.load(f.read(), Loader=yaml.Loader)

        # read the eufs-master/src directories for groups and their repos
        group_info = {}  # dict{group name : [group repo]}
        groups_root_src = os.path.join(eufs_master, "src")
        if os.path.exists(groups_root_src):
            # read each group's repos
            for group_name in os.listdir(groups_root_src):
                group_dir = os.path.join(groups_root_src, group_name)
                group_repos = []
                for group_repo in os.listdir(group_dir):
                    group_repo_dir = os.path.join(
                        groups_root_src, group_name, group_repo)
                    group_repos.append(group_repo_dir)
                group_info[group_name] = group_repos
        else:
            # even if path does not exist, all and single repos should work fine
            logger.info(Fore.RED + groups_root_src
                        + " does not exist. "
                        + "Relying on repos.yaml only and thus cannot view group repos. "
                        + Fore.RESET)

        if args.skip:
            logger.info(
                Fore.RED +
                "Skip command is not implemented for now and will be ignored"
                + Fore.RESET)

        planned_tests = []
        all_outputs = []
        all_tested_packages = []

        if args.packages == []:
            # test on all with respect to ignore list
            logger.info("Received " + Fore.BLUE + "none" + Fore.RESET +
                        " as package argument, testing all packages")
            path = os.path.join(eufs_master, "src")
            planned_tests.append(path)
        else:
            # foreach, decide if it's a group or repo name, then run test
            for package in args.packages:
                if package in group_info.keys():
                    logger.info("Received " + Fore.BLUE + f"{package}" + Fore.RESET +
                                ", a group name as package argument, "
                                + "testing all packages under this group")
                    path = os.path.join(eufs_master, "src", package)
                    planned_tests.append(path)
                elif package in repo_info:
                    logger.info("Received " + Fore.BLUE + f"{package}" + Fore.RESET +
                                ", a repo name as package argument, testing this package alone")
                    path = repo_info[package]
                    planned_tests.append(path)
                else:
                    logger.info(
                        "Received " + Fore.RED + f"{package}" + Fore.RESET +
                        ", an unknown name as package argument, skipping")
                    continue
        # run the tests for desired packages / under desired paths,
        # checking for valid planned tests
        if len(planned_tests) == 0:
            logger.info(
                Fore.RED +
                "No valid test packages given, check argument: must be group name, "
                + "repo name or nothing. "
                + Fore.RESET)
            return
        for p in planned_tests:
            tested_packages = self.run_tests(args, p, logger)
            all_tested_packages += tested_packages
            if not args.print_to_console:
                outputs = self.fetch_test_results(tested_packages)
                all_outputs += outputs

        if not args.print_to_console:
            report_paths = self.fetch_test_reports(all_outputs, all_tested_packages, logger)
            final_report_xml_path, final_report_html_path = self.generate_report(
                report_paths)
            # can only add clickable link to a html, not to open a directory
            logger.info(
                Fore.BLUE +
                f"Merged xml file created at: {final_report_xml_path}"
                + Fore.RESET)
            # using an escape sequence
            logger.info(
                Fore.BLUE +
                "Ctrl + Click to view the "
                + f"\x1b]8;;file://{final_report_html_path}\a/HTML/\x1b]8;;\a report. "
                + Fore.RESET)

    def run_tests(self, args, target_path: str, logger: logging.Logger, build=True) -> list:
        """
        Runs the commands, including build and test,
        returns a list of tested packages' names.
        """
        eufs_master = os.environ["EUFS_MASTER"]
        # # unimplemented skip command:
        # skip_command = "--packages-skip " + " ".join(args.skip)
        # if not args.all:
        #     # Load blacklist if it exists
        #     blacklist_path = os.path.join(os.environ["EUFS_MASTER"], ".eufscli", "eufsbuild.yaml")
        #     if os.path.exists(blacklist_path):
        #         with open(blacklist_path, "r") as f:
        #             blacklist = yaml.load(f.read(), Loader=yaml.Loader)["skip"]
        #     else:
        #         blacklist = []
        #     skip_command += " " + " ".join(blacklist)
        if build:
            Popen([
                "colcon", "build", "--base-paths", target_path
            ], cwd=eufs_master, stdout=DEVNULL).wait()
        cmd = self.build_test_command(target_path, args.print_to_console, args.regex)
        test_process = subprocess.run(cmd, cwd=eufs_master, stdout=PIPE, stderr=PIPE)

        output_text = test_process.stdout.decode("utf8")
        if args.print_to_console:
            logger.info(output_text)

        tested_packages = self.fetch_test_packages(output_text, logger)
        return tested_packages

    def build_test_command(self, target_path: str, print_results: bool, regex: list) -> list:
        cmd = ["colcon", "test", "--base-paths", target_path]
        if print_results:
            cmd += ["--event-handlers", "console_direct+"]
        if len(regex) > 0:
            cmd += ["--ctest-args", "-R", "|".join(regex)]
        return cmd

    def fetch_test_results(self, tested_packages):
        """
        Runs the command `colcon test-result` for every tested packages
        and returns the commandline output in a list.
        """
        test_results = []
        for tested_package in tested_packages:
            test_result_process = subprocess.run(
                ["colcon", "test-result", "--test-result-base",
                 "build/" + tested_package, "--all"],
                cwd=os.environ["EUFS_MASTER"],
                stdout=PIPE, stderr=PIPE)
            test_results.append(test_result_process.stdout.decode("utf8"))
        return test_results

    def fetch_test_packages(self, output: str, logger: logging.Logger) -> list:
        """
        Finds the list of tested packages by finding all lines
        with "Starting:" and get the corresponding name
        """
        test_packages = []
        splits = output.split("\n")
        for split in splits:
            if "Starting >>>" in split:
                # "Starting >>> eufs_msgs" => "eufs_msgs"
                package_name = split.split(">>> ")[1]
                test_packages.append(package_name)
        return test_packages

    def fetch_test_reports(self, all_outputs: list, all_tested_pkgs: list,
                           logger: logging.Logger) -> list:
        """
        Gets the paths of each test reports.
        """
        # paths of the test reports file; should be all under $EUFS_MASTER:
        test_report_paths = []
        longest_pkg_name_len = 0
        for pkg in all_tested_pkgs:
            if len(pkg) > longest_pkg_name_len:
                longest_pkg_name_len = len(pkg)
        for i in range(len(all_outputs)):
            output = all_outputs[i]
            pkg_title = all_tested_pkgs[i]
            space_holder = " " * (longest_pkg_name_len - len(pkg_title) + 1)
            test_report_path = []
            splits = output.split("\n")
            for split in splits:
                path = split.split(":")[0]
                if path[:5] == "build":
                    test_report_path.append(path)
                elif path[:7] == "Summary":
                    logger.info(Fore.BLUE +
                                "[" + f"{pkg_title}" + "] " + space_holder
                                + f"{split}" + Fore.RESET)
            # remove duplicates and preserve ordering:
            test_report_path = list(
                {k: None for k in test_report_path}.keys()
            )
            test_report_paths.append(test_report_path)
        return test_report_paths

    def uniquify(self, path):
        filename, extension = os.path.splitext(path)
        counter = 1

        while os.path.exists(path):
            path = filename + " (" + str(counter) + ")" + extension
            counter += 1

        return path

    def create_folders(self,
                       time_stamped_folder_path,
                       soft_sym_link_folder):
        """
        Creates the time stamped eufstest folder and the symlink folder.
        """
        if os.path.exists(time_stamped_folder_path):
            uniquified_name = self.uniquify(time_stamped_folder_path)
            logging.getLogger("eufs_cli.test").info(
                Fore.RED + "Tests are too fast! Adding a counter to the new folder: "
                + Fore.RESET
                + uniquified_name
            )
            os.makedirs(uniquified_name)
        else:
            os.makedirs(time_stamped_folder_path)
        # update the latest soft symlink to point to this run;
        # update by delete and create
        if os.path.exists(soft_sym_link_folder):
            os.remove(soft_sym_link_folder)
        symlink(time_stamped_folder_path, soft_sym_link_folder, target_is_directory=True)

    def merge_xml_reports(self, report_paths_lists, overall_xml_path):
        # merge reports:
        overall_xml = None
        logger = logging.getLogger("eufs_cli.test")
        for report_paths in report_paths_lists:
            for path in report_paths:
                # check if it is junit xml, yes ->
                # check if overall_xml is written something:
                # yes -> append/merge, no -> assign as overall_xml
                # first skips all Test.xml file:
                ultimate_file_name = path.split("/")[-1]
                if ultimate_file_name == "Test.xml":
                    continue
                cur_full_path = os.path.join(os.environ["EUFS_MASTER"], path)
                try:
                    cur_xml = JUnitXml.fromfile(cur_full_path)
                    if overall_xml is None:
                        overall_xml = cur_xml
                    else:
                        overall_xml += cur_xml
                except junitparser.junitparser.JUnitXmlError:
                    # not a junit xml file, skip
                    logger.info(
                        Fore.RED + f"{cur_full_path}" +
                        Fore.RESET + "is not a junit xml file, skipping...")
        logger.info("All xml merged")
        overall_xml.write(overall_xml_path)

    def generate_report(self, report_paths_lists: list,
                        path_xml_template="log/eufstest_",
                        report_name_template="eufstest_report.xml"):
        """
        Merges all report.xmls, Stores them under $EUFS_MASTER/log/eufstest_[time_stamp],
        Converts them to html, Creates soft symlink `latest_eufstest` pointing to the
        current one, Displays the path to the test reports and a clickable link to the
        generated html report.
        """
        # store: under $EUFS_MASTER/log/eufstest_[time_stamp];
        # create soft symlink latest_eufstest pointing to this one
        eufs_master = os.environ["EUFS_MASTER"]
        cur_datetime = datetime.datetime.now()
        cur_folder_name = path_xml_template + \
            self.datetime_formatter(cur_datetime)
        # create all folders
        time_stamped_folder_path = os.path.join(eufs_master, cur_folder_name)
        overall_xml_path = os.path.join(
            eufs_master, cur_folder_name, report_name_template)
        soft_sym_link_folder = os.path.join(
            eufs_master, "log", "latest_eufstest")
        self.create_folders(time_stamped_folder_path,
                            soft_sym_link_folder)

        self.merge_xml_reports(report_paths_lists, overall_xml_path)

        # generate html report:
        overall_html_path = overall_xml_path[:-4] + ".html"
        runner.run([
            overall_xml_path, overall_html_path
        ])

        return overall_xml_path, overall_html_path

    def datetime_formatter(self, time: datetime.datetime) -> str:
        """
        Converts the time to the format as yyyy-mm-dd_hh-mm-ss.
        """
        return time.strftime("%Y-%m-%d_%H-%M-%S")
