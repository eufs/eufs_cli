import logging
import os
from subprocess import PIPE, Popen
from colorama import Fore

import yaml

from eufscli import CommandExtension


class EUFSBuild(CommandExtension):
    """
    Build packages (wrapper for colcon).

    The main ways of specifying the packages that you want built are:
    1. Simply running `eufs build` will build all packages unless they are
       part of the list of packages to be skipped in $EUFS_BUILD_IGNORELIST
       (or in $EUFS_MASTER/.eufscli/eufsbuild.yaml if ignorelist not specified).
       An example of the format of such a yaml file can be found in
       eufsbuild/config/eufsbuild.yaml.
    2. Running `eufs build <packages>` will build all packages in the space
       separated list even if they are part of the ignorelist.
    3. Running `eufs build -a` will build all packages, disregarding the ignorelist.
    4. Running `eufs build -a -s <packages>` will build all packages unless
       specified in the space separated list after '-s'.
    5. Running `eufs build -s <packages>` will build all packages unless they
       are in the ignorelist or in the space separated list after '-s'.
    6. Running `eufs build -r <repo_names>` will build all packages within the
       space separated list of repositories. Can not be used with other flags
       at the moment (for simplicity), though there is no reason why this
       cannot be changed in the future.

    All other combinations are illegal. `--symlink-install` is automatically
    passed to colcon.
    """

    def configure(self, parser):
        parser.add_argument(
            "packages",
            nargs="*",
            default=[],
            help="Space separated list of packages.",
        )
        parser.add_argument(
            "-a",
            "--all",
            action="store_true",
            dest="all",
            help="Disregard the ignorelist",
        )
        parser.add_argument(
            "-s",
            "--skip",
            nargs="*",
            dest="skip",
            default=[],
            help="Space separated list of packages to skip.",
        )
        parser.add_argument(
            "-r",
            "--repos",
            nargs="*",
            dest="repos",
            default=[],
            help="Build packages within specified repositories.",
        )
        parser.add_argument(
            "--release",
            action="store_true",
            dest="release",
            help="Build packages with CMAKE_BUILD_TYPE=Release",
        )

        parser.add_argument(
            "-c", "--continue", action="store_true", dest="continue_on_error", help="continue error"
        )
        parser.add_argument(
            "--parallel-workers",
            nargs=1,
            dest="parallel_workers",
            help="The maximum number of packages to process in parallel, or '0' for no limit."
        )

    def main(self, args):
        build_prep = "cd $EUFS_MASTER"
        options = ["--symlink-install", "--cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON"]
        env = os.environ.copy()
        env["PYTHONWARNINGS"] = "ignore::UserWarning"

        # Get logger
        logger = logging.getLogger("eufs_cli.build")

        # Is not production if EUFS_PRODUCTION is not set or is set to False
        is_production = os.getenv("EUFS_PRODUCTION", "False").lower() not in ("false", "0", "f")
        if args.release or is_production:
            logger.info(Fore.GREEN + "Release mode activated!" + Fore.RESET)
            options.append("--cmake-args -DCMAKE_BUILD_TYPE=Release")

        if args.continue_on_error:
            options.append("--continue-on-error")

        if args.parallel_workers:
            options.append("--parallel-workers " + args.parallel_workers[0])

        # Load ignorelist if it exists
        default_ignorelist_path = os.path.join(
            os.environ["EUFS_MASTER"], ".eufscli", "eufsbuild.yaml"
        )
        ignorelist_path = os.environ.get("EUFS_BUILD_IGNORELIST", default_ignorelist_path)
        ignorelist = []
        if os.path.exists(ignorelist_path):
            with open(ignorelist_path, "r") as f:
                ignorelist = yaml.load(f.read(), Loader=yaml.Loader)["skip"]

        if args.repos != []:
            # Get list of repositories
            eufs_master = os.environ["EUFS_MASTER"]
            repo_info_path = os.path.join(eufs_master, ".eufscli", "repos.yaml")

            # Read yaml file with eufs repository paths
            with open(repo_info_path, "r") as f:
                repo_info = yaml.load(f.read(), Loader=yaml.Loader)

            # Initialize dict
            packages_dict = {}
            for repo in repo_info:
                if repo != "eufs-master":
                    packages_dict[repo] = []

            # All packages must have a package.xml file
            proc = Popen(
                "find $EUFS_MASTER/src -name package.xml",
                shell=True,
                stdout=PIPE,
            )
            proc.wait()
            output = proc.stdout.read().decode()

            # Process package list
            packages = output.split("\n")
            packages = list(filter(None, packages))  # Remove empty entries
            for package in packages:
                package_path = package.split("/")
                # Package belongs to repo that shows up last in the path
                for path_segment in package_path[::-1]:
                    if path_segment in packages_dict:
                        # Package name is one segment above package.xml
                        packages_dict[path_segment].append(package_path[-2])
                        break

            # Create list of packages to build
            packages_select = "--packages-select "  # Note the space
            for repo in args.repos:
                # Check to make sure repos are valid
                if repo not in packages_dict:
                    raise Exception(f"{repo} not a valid repo.")
                else:
                    packages_select += " ".join(packages_dict[repo]) + " "

            Popen(
                f"{build_prep} && colcon build {' '.join(options)} {packages_select}",
                shell=True,
                env=env
            ).wait()

        elif args.all:
            # You can't build all packages and select packages to build!
            if args.packages != []:
                raise Exception("'-a' can only be used in conjunction with '-s'.")
            # Build all packages
            elif args.skip == []:
                Popen(
                    f"{build_prep} && colcon build {' '.join(options)}",
                    shell=True,
                    env=env
                ).wait()
            # Build packages as long as they are not in the skip list
            elif args.skip != []:
                packages_skip = "--packages-skip " + " ".join(args.skip)
                Popen(
                    f"{build_prep} && colcon build {' '.join(options)} {packages_skip}",
                    shell=True,
                    env=env
                ).wait()
        # Can't select which packages to build and then skip them!
        elif args.packages != [] and args.skip != []:
            raise Exception("'-s' cannot be used when specifying individual packages.")
        # Build packages in the packages list
        elif args.packages != []:
            packages_select = "--packages-select " + " ".join(args.packages)
            Popen(
                f"{build_prep} && colcon build {' '.join(options)} {packages_select}",
                shell=True,
                env=env
            ).wait()
        # Build packages unless they're in skip or ignorelist
        elif args.skip != []:
            packages_skip = "--packages-skip " + " ".join(args.skip) + " " + " ".join(ignorelist)
            Popen(
                f"{build_prep} && colcon build {' '.join(options)} {packages_skip}",
                shell=True,
                env=env
            ).wait()
        # Build packages unless they're in the ignorelist
        else:
            packages_skip = "--packages-skip " + " ".join(ignorelist)
            Popen(
                f"{build_prep} && colcon build {' '.join(options)} {packages_skip}",
                shell=True,
                env=env
            ).wait()
