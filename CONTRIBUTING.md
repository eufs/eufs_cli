# eufscli

This file provides information for developers looking to contribute to the eufscli project.

## Development Environment

I would recommend using [`pyenv`](https://github.com/pyenv/pyenv) and [`pyenv-virtualenv`](https://github.com/pyenv/pyenv-virtualenv) to create a clean new Python environment for development. The instructions displayed below are taken from the installation sections of the respective repositories. Check there for the most up-to-date installation instructions.

1. To do this, first install the required system dependencies (needed for building Python):

   ```bash
   $ sudo apt-get update; sudo apt-get install make build-essential libssl-dev zlib1g-dev \
   libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
   libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
   ```

2. Next, we can clone `pyenv` using the `pyenv-installer`. Run the following in your terminal:

   ```bash
   $ curl https://pyenv.run | bash
   ```

3. Then add the following lines into your `~/.profile` (must be above where you source `~/.bashrc`):

   ```bash
   export PYENV_ROOT="$HOME/.pyenv"
   export PATH="$PYENV_ROOT/bin:$PATH"
   ```

   And add the following command to the end of your `~/.profile`:

   ```bash
   eval "$(pyenv init --path)"
   ```

   Add the lines into your `~/.bash_profile` as well if you have it.

   If you're using zsh, do the same thing as above, but put them in your `~/.profile` and `~/.zprofile`.

4. Next, add these commands to your `~/.bashrc` (or `~/.zshrc`) file:

   ```bash
   eval "$(pyenv init -)"
   eval "$(pyenv virtualenv-init -)"
   ```

5. Next, logout of your session and log back in. This is needed for the changes to take effect.

6. Install Python 3.8.5 using `pyenv`:

   ```bash
   $ pyenv install 3.8.5
   ```

7. Create a new virtual environment called `eufs_cli` with `pyenv` by running in your terminal:

   ```bash
   $ pyenv virtualenv 3.8.5 eufs_cli
   ```

8. In the directory where you cloned this repository run in your terminal:
   ```bash
   $ pyenv local eufs_cli
   ```
   This will set this repository to use the eufs_cli virtual environment every time you `cd` into it.

Now, everytime you change into the eufs_cli directory, your `eufs_cli` Python environment should start automatically. Within this environment you should use the commands `python` and `pip`.

## Testing During Development

During development, you will likely want to test what you have written. This can easily be done by running the `dev-install.sh` script **in the root of this repository**:

```bash
./dev-install.sh
```

**Note: This should only need to be run when `setup.py` or `setup.cfg` files are modified.**

Note that this project has [pre-commit](https://pre-commit.com/) set up to run several tests before allowing you to commit your code. These, currently, are only formatting checks. It may be expanded to actual tests in the future (e.g with `PyTest`). Setting this up was done with reference to [this blog post](https://mitelman.engineering/blog/python-best-practice/automating-python-best-practices-for-a-new-project/).

## Creating a New Command

`eufscli` is created to be very easy to extend with new commands. Every command you see when you do `eufs -h` is in fact its own package. Hence, adding new commands is just a matter of creating and configuring a new Python package. We'll see how to do this in this section.

### Python Package Files

The first step in creating a new command, it to create a new Python package. The easiest place to put the package is in this directory with all the other packages.

A Python package should have 3 files associated with it:

1. `setup.py`

   This file is very short and contains a single `setup()` method call imported from `setuptools`. This is not strictly required. However, the [`setuptools` documentation states](https://setuptools.readthedocs.io/en/latest/userguide/declarative_config.html):

   > If compatibility with legacy builds (i.e. those not using the PEP 517 build API) is desired, a `setup.py` file containing a `setup()` function call is still required even if your configuration resides in `setup.cfg`.

   Hence, we include a `setup.py` file for this reason.

2. `setup.cfg`

   This contains all of our configuration for the Python package. If you are familiar with using `setup()` then most of the configuration is very similar, just in a different format. For more details see the [`setuptools` documentation](https://setuptools.readthedocs.io/en/latest/userguide/quickstart.html).

   The most important aspect of these files is the section under `[options.entry_points]`. Entry points are used by `eufscli` to discover your command and integrate it under the `eufs` executable.

   To register your command as a top-level command, you need to add the following to your `setup.cfg` file:

   ```toml
   [options.entry_points]
   eufscli.command =
      <command_name> = <package_name>.<sub_package_name>.<file_name>:<class_name>
   ```

   Replacing everything in `<>` with the appropriate strings. Note that if your file is not in a `sub-package`, then simply omit it from the above template.

   What the above config snippet does is make your class discoverable by the `eufscli` program when it searches for all the commands under the `eufscli.command` entry point. Hence, you should see your command show up along side the pre-existing ones when running, after building.

   A very similar thing is done for verbs, but we will get to that at later on.

3. `pyproject.toml`

   This file is used to specify what program to use for packing up the Python package. See the [`setuptools` documentation](https://setuptools.readthedocs.io/en/latest/build_meta.html) for more information.

   When creating new packages, one can simply copy this file from any of the existing packages in this directory.

### The Command Itself

The code for you command lives in a directory with the same name as the top-level directory. For example, if you look inside the `eufsreset` directory, you'll see that there is another directory called `eufsreset`. The second `eufsreset` is actually the Python package and is what the `<package_name>` refers to in the `setup.cfg` file.

So, like the `eufsreset` package, you'll create a `<your_package_name>` directory inside your `<your_package_name>` directory. The file hierarchy should be something like:

```
- your_package
    - your_package
        - __init__.py
    - pyproject.toml
    - setup.cfg
    - setup.py
```

The the top-level `<your_package>` can be in this directory.

Within the `<your_package>` package (i.e the second `<your_package>` directory), you need the `__init__.py` file to create the actual file, and one main file to be the entry point into your application. Let's call it `main.py`.

Within `main.py` we need to add some minimal boiler-plate code to set things up. The simplest command to implement would some something like:

```python
from eufscli import CommandExtension


class Hello(CommandExtension):
    """Prints 'hello' when called."""

    def main(self, args):
       # This method is what gets invoked when your command is
       # executed from the command line.
       print("Hello")

```

This command, when invoked on the command line, would simply print 'hello' to the terminal.

To be able to actually run this command in your terminal, you would need the following file structure (the names are not important as long as they are consistent):

```
- your_package
    - your_package
        - __init__.py
        - main.py
    - pyproject.toml
    - setup.cfg
    - setup.py
```

and in our `setup.cfg` file we would have the entry_point configured as:

```toml
[options.entry_points]
eufscli.command =
   hello = your_package.main:Hello
```

Then you, after installing the package via `install.sh` (or `dev-install.sh` for development purposes), should be able to run `eufs hello` and see your results.

Let's break down the code and highlight some important aspects.

1.  ```python
    from eufscli import CommandExtension
    ```

    `CommandExtension` is the base class from which all top-level commands (i.e commands that show up when you run `eufs -h` which is equivalent to all classes listed under `eufscli.command`) should inherit from.

2.  ```python
    class Hello(CommandExtension):
    ```

    This simply creates a class that inherits from the `CommandExtension` class. Note that the name of the class should be the same as the on in `setup.cfg`.

3.  ```python
    """Prints 'hello' when called."""
    ```

    Docstrings are actually important in this case.

    When you do `eufs -h` you will see the command show up as well as a short sentence next to it, describing the command.

    What's happening here is that the `eufscli` program takes the first line of your docstring and places it in the help message. Hence the first line of your docstring should be short and written whilst keeping in mind that it will show up when you run `eufs -h`.

4.  ```python
    def main(self, args):
    ```

    This defines the method that gets run when your command is invoked on the command line. In this particular case, this `main` method will get called when the user runs `eufs hello` on their terminal.

    The `args` parameter will store any commandline arguments that are passed along with your command. These are defined in the `configure` method explained below.

5.  ```python
    def configure(self, parser):
    ```

    It's not shown in the example above, but there is another method that `CommandExtension` defines. It's the `configure` command. This gets run by `eufscli` before parsing the command entered by the user.

    By default its implementation does nothing but you can use it to define arguments and flags for your command. You are given a parameter `parser` which is a [`argparse` parser object](https://docs.python.org/3.8/library/argparse.html) with which you can do things like call `add_argument()` and things like that.

    Once you do this and call your command with the `-h` flag, you should see your arguments and flags show up in the help message.

I strongly suggest you look through existing command implementations to see how things are done. Also, have a play around! Because of the modularity of the architecture, if you mess something up, you can just delete the package and create a new one! If you want even more examples, take a look at the [ros2cli repository](https://github.com/ros2/ros2cli/) which `eufscli` is heavily based on.

### Verbs

Sometimes, you may want to create another level of commands. For example, you may want something like `eufs hello <sub-command>`. This can be done in the same way as the top-level commands.

All you need to do is add the following to your `configure()` method:

```python
register_entry_point(parser, self.CLI_NAME, 'verb', 'eufs<package_name>.verb')
```

Then in your `setup.cfg` file, you can add just below your `eufscli.command` entry:

```toml
eufs<package_name>.verb =
   <sub_command> = <package_name>.<sub_package_name>.<file_name>:<class_name>
```

Notice how similar this is to what we discussed previously. The only real difference is the `eufs<package_name>.verb` vs `eufscli.command`. In all honesty, you change the string `eufs<package_name>.verb` to anything you want as long as they're consistent, but having some convention will make things easier to understand.

The class for our sub-command is identical to the command class in all regards except they inherit from `VerbExtension` instead of `CommandExtension`.

You can now call the main member function of your sub-command class from within the main function of the command class.

Expanding on the example above with the `hello` command, we can add a verb to print out "How are you?" in `main.py` as follows

```python
from eufscli import CommandExtension
from eufscli import VerbExtension


class Hello(CommandExtension):
    """Prints 'Hello' when called."""
    def configure(self, parser):
        # Register an entry point to use verbs. This can be done once
        # to use as many verbs as you can code
        register_entry_point(parser, self.CLI_NAME, 'verb', 'your_package.verb')

    def main(self, args):
       print("Hello")

       # Call the main function of a verb if one is given
       if hasattr(args, 'verb'):
           extension = getattr(args, 'verb')
           return extension.main(args=args)


class AskVerb(VerbExtension):
    """Asks 'How are you?' when called"""
    def main(self, args):
       print("How are you?")
```

The `setup.cfg` file would then look like this

```toml
your_package.verb =
    ask = your_package.main:AskVerb
```
