import logging
import os
from subprocess import run
import fnmatch

from colorama import Fore
import shutil
import itertools
from pathlib import Path

from eufscli import CommandExtension


class EUFSLint(CommandExtension):
    """
    Performs the same linting check as the CI does, and more importantly, can fix errors for you!.
    """

    def configure(self, parser):
        parser.add_argument(
            "path",
            nargs="*",
            action="store",
            help="paths in which files should be linted"
        )
        parser.add_argument(
            "--fix",
            action="store_true",
            dest="fix",
            help="whether to fix errors or report them"
        )
        parser.add_argument(
            "-l",
            "--languages",
            nargs="*",
            dest="languages",
            default=["cpp", "python"],
            help="Files in which languages to lint.",
        )

    def find_files(self, filename_regex, directories, files):
        paths = itertools.chain.from_iterable(Path(path).glob(
            f"**/{filename_regex}") for path in directories)
        paths = itertools.chain(
            paths, (file for file in files if fnmatch.fnmatch(file, filename_regex)))
        paths = " ".join(str(path) for path in paths)
        return paths

    cpp_regex = r"*.[hc]pp"
    python_regex = r"*.py"

    def main(self, args):
        logger = logging.getLogger("eufs_cli.lint")
        eufs_path = os.environ["EUFS_MASTER"]

        current_directory = os.getcwd()
        directories = [current_directory] if len(args.path) == 0 else \
            [os.path.join(current_directory, path) for path in args.path if os.path.isdir(path)]
        files = [path for path in args.path if os.path.isfile(path)]

        cpp_files = self.find_files(self.cpp_regex, directories, files)
        python_files = self.find_files(self.python_regex, directories, files)

        lint_cpp_condition = "cpp" in args.languages and len(cpp_files) > 0
        if args.fix and lint_cpp_condition:
            run(f"clang-format -i {cpp_files}", shell=True)
            logger.info(Fore.GREEN + "Finished Linting C++ files!" + Fore.RESET)
        elif lint_cpp_condition:
            config_filename = "CPPLINT.cfg"
            config_path = os.path.join(eufs_path, config_filename)
            shutil.copyfile(config_path, config_filename)

            lint_ignores = os.environ.get("LINT_IGNORES") or []
            cpp_lint_exclude_flags = ' '.join([f"--exclude {ignore}" for ignore in lint_ignores])
            run(f"python3 -m cpplint --recursive {cpp_lint_exclude_flags} {cpp_files}", shell=True)

            os.remove(config_filename)

        lint_python_condition = "python" in args.languages and len(python_files) > 0

        config_path = os.path.join(eufs_path, ".flake8")
        if args.fix and lint_python_condition:
            run(f"autopep8 {python_files} --in-place --global-config {config_path}", shell=True)
            logger.info(Fore.GREEN + "Finished Linting Python files!" + Fore.RESET)
        elif lint_python_condition:
            run(f"python3 -m flake8 {python_files} --config {config_path}", shell=True)
