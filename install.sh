#!/usr/bin/env bash

# We do this to make sure we use the Python accessible in the home directory
# Important if you have a separate Python environment in eufs_cli. If you
# do then the cli will get installed only for that environment, and so
# you cannot use it outside that environment which is not what you want.
cd $HOME

# Install eufscli first because all other packages depend on it
pip3 install $EUFS_CLI_HOME/eufscli

# Loop through the $EUFS_CLI_HOME directory for `eufs*` packages and install them
for d in $EUFS_CLI_HOME/eufs*; do
    if test -f "$d/setup.py"; then
        pip3 install $d/
    fi
done
