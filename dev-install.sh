# Loop through current directory for `eufs*` packages and install them
for d in eufs*; do
    if test -f "$d/setup.py"; then
        pip install --editable $d[dev]
    fi
done
