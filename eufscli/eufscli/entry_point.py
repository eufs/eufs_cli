import argparse
import logging

from pkg_resources import iter_entry_points

from eufscli.utils import get_doc, get_first_line_doc


def register_entry_point(parser, cli_name, dest, entry_point_name, command=None):
    # add subparser to root parser to enable creation of commands
    subparser = parser.add_subparsers(
        title="Commands",
        dest=dest,
        metavar="<command>",
        help="description",
        description=f"Use `{cli_name} <command> -h` for more detailed help",
    )

    command_parsers = {}
    command_extensions = instantiate_extensions(cli_name, entry_point_name, command)

    if not command_extensions.keys():
        command_extensions = instantiate_extensions(cli_name, entry_point_name)

    if command_extensions:
        for name in sorted(command_extensions.keys()):
            # get command extension class of current entry point
            extension = command_extensions[name]

            # create and configure subparser for each command
            command_parser = subparser.add_parser(
                name,
                formatter_class=argparse.RawDescriptionHelpFormatter,
                description=get_doc(extension),
                help=get_first_line_doc(extension),
            )
            command_parser.set_defaults(**{dest: extension})
            command_parsers[name] = command_parser

            # configure the command arguments to populate help command
            extension.configure(command_parser)

    return command_parsers


def instantiate_extensions(cli_name, group_name, command=None):
    """Instantiate every class from the entry point group."""

    extension_types = load_entry_points(group_name, command)
    extension_instances = {}
    for extension_name, extension_class in extension_types.items():
        extension = extension_class()
        extension.NAME = extension_name
        extension.CLI_NAME = cli_name
        extension_instances[extension_name] = extension

    return extension_instances


def load_entry_points(group_name, command=None):
    """Load the entry points for a specific group."""
    logger = logging.getLogger("eufs_cli")
    extension_types = {}
    for entry_point in iter_entry_points(group=group_name, name=command):
        try:
            extension_types[entry_point.name] = entry_point.load()
        except Exception as e:
            logger.error(f"Failed to load entry point `{entry_point.name}`: {e}")

    return extension_types
