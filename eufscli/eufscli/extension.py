class CommandExtension:
    """
    The interface for `command` extensions.

    The following properties must be defined:
    * `CLI_NAME` (will be set to the program name)
    * `NAME` (will be set to the entry point name)

    The following methods must be defined:
    * `main`

    The following methods can be defined:
    * `configure`
    """

    CLI_NAME = None
    NAME = None

    def __init__(self):
        super(CommandExtension, self).__init__()

    def configure(self, parser):
        pass

    def main(self, args):
        raise NotImplementedError()


class VerbExtension:
    """
    The interface for `verb` extensions.

    The following properties must be defined:
    * `CLI_NAME` (will be set to the program name)
    * `NAME` (will be set to the entry point name)

    The following methods must be defined:
    * `main`

    The following methods can be defined:
    * `configure`
    """

    CLI_NAME = None
    NAME = None

    def __init__(self):
        super(VerbExtension, self).__init__()

    def configure(self, parser):
        pass

    def main(self, args, **kwargs):
        raise NotImplementedError()
