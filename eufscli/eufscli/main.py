import argparse
import logging
import os
import signal
import sys

import argcomplete

from eufscli import register_entry_point


def main():
    # Set SIGINT to trigger KeyboardInterrupt exception
    signal.signal(signal.SIGINT, signal.default_int_handler)

    # Setup logging
    logger = logging.getLogger("eufs_cli")
    logger.setLevel(logging.DEBUG)
    logger.propagate = False

    # File Handler
    logfile_path = os.path.join(os.environ["EUFS_MASTER"], ".eufscli", "log", "eufs_cli.log")
    os.makedirs(os.path.dirname(logfile_path), exist_ok=True)
    handler = logging.FileHandler(logfile_path, "a")
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s-%(name)s-%(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    # Stream Handler
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Check to make sure proper environment variables are set
    env_check()

    # Check to make sure config directory exists
    init_config_dir()

    # top level parser
    cli_name = "eufs"
    description = f"{cli_name} is an easily extensible command-line tool " "developed by EUFS-AI"
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    # register entry points as commands
    extension_key = "command"
    entry_point_name = "eufscli.command"

    if len(sys.argv) >= 2 and sys.argv[1] not in ["-h", "--help"]:
        command = sys.argv[1]
        register_entry_point(parser, cli_name, extension_key, entry_point_name, command)
    else:
        register_entry_point(parser, cli_name, extension_key, entry_point_name)

    # register argcomplete for autocompletion
    argcomplete.autocomplete(parser)

    # parse the command line arguments (this is done automatically):
    # >> In a script, parse_args() will typically be called with no arguments,
    # >> and the ArgumentParser will automatically determine the command-line
    # >> arguments from sys.argv.
    args = parser.parse_args()

    # get the subcommand to use (stored in args.extension_key)
    extension = getattr(args, extension_key, None)

    # deal with the case that no command was passed
    if extension is None:
        parser.print_help()
        return 0

    # call the main method of the subcommand
    extension.main(args)


def env_check():
    # Make sure EUFS_MASTER environment variable is set
    if "EUFS_MASTER" not in os.environ:
        raise Exception(
            "Please set the EUFS_MASTER environment variable " + "to the eufs-master repository."
        )

    # Make sure EUFS_CLI_HOME environment variable is set
    if "EUFS_CLI_HOME" not in os.environ:
        raise Exception(
            "Please set the EUFS_CLI_HOME environment variable " + "to the eufs-cli repository."
        )


def init_config_dir():
    # Create .eufscli directory in eufs-master if it doesn't exist
    cli_config_dir = os.path.join(os.environ["EUFS_MASTER"], ".eufscli")
    if not os.path.isdir(cli_config_dir):
        os.mkdir(cli_config_dir)
