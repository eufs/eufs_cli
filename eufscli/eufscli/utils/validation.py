def input_yn(prompt, logger):
    """
    Standard [Y/n] input validation
    """
    error_msg = "Warning: Invalid input, please input [Y/n]"
    try:
        response = str(input(f"{str(prompt)} [Y/n]: ")).lower().strip()
        if response in ["", "y", "yes", "1", "true"]:
            return True
        elif response in ["n", "no", "0", "false"]:
            return False
        else:
            logger.warning(error_msg)
            return input_yn(prompt, logger)
    except Exception as error:
        logger.warning(error)
        logger.warning(error_msg)
        return input_yn(prompt, logger)


def input_int(prompt, logger):
    """
    Standard integer input validation
    """
    error_msg = "Warning: Invalid input, please input an integer"
    try:
        response = int(input(f"{prompt}: "))
        return response
    except Exception:
        logger.warning(error_msg)
        return input_int(prompt, logger)
