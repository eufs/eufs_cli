def get_first_line_doc(any_type):
    """Get the first line of the docstring."""

    if not any_type.__doc__:
        return ""
    lines = any_type.__doc__.splitlines()
    if not lines:
        return ""
    if lines[0]:
        line = lines[0]
    elif len(lines) > 1:
        line = lines[1]
    return line.strip().rstrip(".")


def get_doc(any_type):
    """Get the docstring."""

    if not any_type.__doc__:
        return ""

    lines = any_type.__doc__.splitlines()
    lines = [line.strip() for line in lines]

    if not lines:
        return ""

    if not lines[0]:
        lines = lines[1:]

    doc = ""
    for line in lines:
        doc += line + "\n"

    return doc
