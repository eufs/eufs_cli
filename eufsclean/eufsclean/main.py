import logging
import os
import shutil
from os.path import isdir

from colorama import Fore

from eufscli import CommandExtension


class EUFSClean(CommandExtension):
    """
    Remove build and install directories for a specific package or the whole workspace.
    """

    def configure(self, parser):
        parser.add_argument("packages", nargs="*", help="Packages to clean")

        parser.add_argument(
            "-a", "--all", action="store_true", dest="all", help="Clean all packages"
        )

    def main(self, args):
        # Get logger
        logger = logging.getLogger("eufs_cli.clean")

        EUFS_MASTER = os.environ["EUFS_MASTER"]
        INSTALL = os.path.join(EUFS_MASTER, "install")
        BUILD = os.path.join(EUFS_MASTER, "build")

        if args.all:
            # Remove INSTALL directory
            if isdir(INSTALL) and isdir(BUILD):
                shutil.rmtree(f"{EUFS_MASTER}/install")
                shutil.rmtree(f"{EUFS_MASTER}/build")
                logger.info(Fore.GREEN + "Removed INSTALL and BUILD directories" + Fore.RESET)
            elif isdir(INSTALL):
                shutil.rmtree(f"{EUFS_MASTER}/install")
                logger.info(Fore.GREEN + "Removed INSTALL directory" + Fore.RESET)
            elif isdir(BUILD):
                shutil.rmtree(f"{EUFS_MASTER}/build")
                logger.info(Fore.GREEN + "Removed BUILD directory" + Fore.RESET)
            else:
                logger.warn(Fore.RED + "BUILD and INSTALL directories not found" + Fore.RESET)
        else:
            for package in args.packages:
                package_build = os.path.join(BUILD, package)
                package_install = os.path.join(INSTALL, package)
                if isdir(package_build) and isdir(package_install):
                    shutil.rmtree(f"{EUFS_MASTER}/install/{package}")
                    shutil.rmtree(f"{EUFS_MASTER}/build/{package}")
                    logger.info(
                        Fore.GREEN
                        + f"Removed INSTALL and BUILD directories for {package}"
                        + Fore.RESET
                    )
                elif isdir(package_build):
                    shutil.rmtree(f"{EUFS_MASTER}/build/{package}")
                    logger.info(Fore.GREEN + f"Removed BUILD directory for {package}" + Fore.RESET)
                elif isdir(package_install):
                    shutil.rmtree(f"{EUFS_MASTER}/install/{package}")
                    logger.info(
                        Fore.GREEN + f"Removed INSTALL directory for {package}" + Fore.RESET
                    )
                else:
                    logger.warn(
                        Fore.RED
                        + f"BUILD and INSTALL directories not found for {package}"
                        + Fore.RESET
                    )
