import logging

import subprocess
from subprocess import Popen
import os

from eufscli import VerbExtension


class RunVerb(VerbExtension):

    """
    Runs the simulation

    """

    def main(self, args):
        logger = logging.getLogger("eufs_cli.sim")
        logger.info("Running EUFS Sim...")

        process = Popen(
            ['ros2', 'launch', 'eufs_sim2', 'eufs_sim2.launch.py'],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            text=True,
            env=os.environ
        )

        while True:
            output = process.stdout.readline()
            if process.poll() is not None:
                break
            if output:
                logger.info(output.strip())

        process.terminate()
        logger.info("EUFS Sim terminated.")
