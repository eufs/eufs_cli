import logging

import subprocess
from eufscli import VerbExtension


class ResetVerb(VerbExtension):

    """
    Resets the eufs sim

    """

    def main(self, args):

        logger = logging.getLogger('eufs_cli.sim')
        logger.info('Resetting sim...')

        command = [
            'ros2',
            'service',
            'call',
            '/reset',
            'std_srvs/srv/Trigger'
        ]

        subprocess.run(command, check=True, stderr=subprocess.PIPE, text=True)
