import logging

import subprocess
from eufscli import VerbExtension


class MissionVerb(VerbExtension):

    """
    Sets up a mission in the sim given a mission name

    Example: eufs sim mission skidpad

    """

    def configure(self, parser):

        parser.add_argument(
            'mission',
            help="""Given mission name such as:
                    not_selected, acceleration,
                    skidpad,
                    autocross,
                    track_drive,
                    manual_driving,
                    ads_ebs_test,
                    ads_inspection,
                    ddt_inspection_a,
                    ddt_inspection_b"""
        )

    def main(self, args):

        mission_codes = {
            'not_selected': 0,
            'acceleration': 1,
            'skidpad': 2,
            'autocross': 3,
            'track_drive': 4,
            'manual_driving': 5,
            'ads_ebs_test': 6,
            'ads_inspection': 7,
            'ddt_inspection_a': 8,
            'ddt_inspection_b': 9,
            'ddt_autonomous_demo': 10
        }

        mission_name = args.mission.lower()

        if mission_name in mission_codes:
            mission_code = mission_codes[mission_name]
            logger = logging.getLogger('eufs_cli.sim')
            logger.info('Setting mission...')

        command = [
            'ros2',
            'service',
            'call',
            '/set_mission',
            'eufs_msgs/srv/SetMission',
            f"{{mission: {mission_code}}}"
        ]

        subprocess.run(command, check=True, stderr=subprocess.PIPE, text=True)
