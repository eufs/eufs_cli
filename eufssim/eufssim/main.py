from subprocess import Popen


from eufscli import CommandExtension
from eufscli import register_entry_point
""""
Command for accessing EUFS sim
"""


class EUFSSim(CommandExtension):

    def configure(self, parser):
        register_entry_point(parser, self.CLI_NAME, 'verb', 'eufs_sim.verb')

    def main(self, args):
        extension = getattr(args, "verb", None)
        if extension is not None:
            return extension.main(args=args)

        Popen(["eufs", "sim", "-h"]).wait()
