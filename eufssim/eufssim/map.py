import logging

import subprocess
from ament_index_python import get_package_share_directory
from eufscli import VerbExtension


class MapVerb(VerbExtension):

    """
    Sets up a map in the sim or fetches available map paths.

    Examples:
      - Set a map: eufs sim map competitions/FSUK/2023/trackdrive
      - List maps: eufs sim map -l
      - List maps: eufs sim map -p
    """

    def configure(self, parser):

        parser.add_argument(
            'map_path',
            nargs='?',
            help="Relative path to map within map_lib"
        )

        parser.add_argument(
            '-p', '--paths',
            action='store_true',
            help="Displays available maps"
        )

        parser.add_argument(
            '-l', '--list',
            action='store_true',
            help="Displays available maps"
        )

        parser.add_argument(
            '-a', '--absolute_path',
            action='store_true',
            help="Changes the path to an absolute path"
        )

    def list_maps(self):

        command = [
            'ros2',
            'service',
            'call',
            '/eufs_sim2/get_map',
            'eufs_msgs/srv/GetMap',
            '{}'
        ]

        result = subprocess.run(command, check=True, stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE, text=True)

        if "map_path=" in result.stdout:
            map_paths = result.stdout.split('map_path=')[1]
            map_paths_list = map_paths.strip('[]').replace("'", "").split(",")

            print("The available maps in map_lib are:")

            for map_path in map_paths_list:
                map_path = map_path.strip()
                second_map_lib_idx = map_path.find("map_lib",
                                                   map_path.find("map_lib") + len("map_lib/maps/"))
                if second_map_lib_idx != -1:
                    formatted_path = map_path[second_map_lib_idx +
                                              len("map_lib/maps/"):].rstrip(".csv)]")
                    print(formatted_path)

    def set_map(self, map_path, absolute):

        logger = logging.getLogger('eufs_cli.sim')
        logger.info('Setting map...\n')

        if absolute:
            request_data = map_path
        else:
            request_data = get_package_share_directory('map_lib') + "/maps/" + map_path + ".csv"

        print(request_data+'\n')

        command = [
            'ros2',
            'service',
            'call',
            '/eufs_sim2/file',
            'eufs_msgs/srv/SetString',
            f'{{data: "{request_data}"}}'
        ]

        subprocess.run(command, check=True, stderr=subprocess.PIPE, text=True)

    def main(self, args):

        if args.paths or args.list:
            self.list_maps()

        if args.map_path:
            self.set_map(args.map_path, args.absolute_path)
