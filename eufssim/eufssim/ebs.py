import logging
import subprocess
from eufscli import VerbExtension


class EbsVerb(VerbExtension):

    """
    Runs emergency breaking system in the sim

    """

    def main(self, args):

        logger = logging.getLogger('eufs_cli.sim')
        logger.info('Ebs active...')

        command = [
            'ros2',
            'service',
            'call',
            '/ebs',
            'std_srvs/srv/Trigger'
        ]

        subprocess.run(command, check=True, stderr=subprocess.PIPE, text=True)
