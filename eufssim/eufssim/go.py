import logging
import subprocess

from eufscli import VerbExtension


class GoVerb(VerbExtension):

    """
    Changes State from Ready to Driving

    """

    def main(self, args):

        logger = logging.getLogger('eufs_cli.sim')
        logger.info('Start Driving...')

        command = [
            'ros2',
            'service',
            'call',
            '/go',
            'std_srvs/srv/Trigger'
        ]

        subprocess.run(command, check=True, stderr=subprocess.PIPE, text=True)
