import logging
import os
from subprocess import Popen

import yaml
from colorama import Fore

from eufscli import CommandExtension


class EUFSPull(CommandExtension):
    """
    Pull changes in all submodules.

    Basically just runs `git pull` in every submodule.

    The user can decide which submodules to apply the `pull` operation
    to. This is specified in the `group` positional argument. This argument
    takes either a repository name, in which case only that repository
    will get the `pull` applied to it, or a repository group, that all
    repos under that group will be pulled. Groups are read in dynamically
    as directories under `$EUFS_MASTER/src/`. Group names take precedence
    over repo names in case there is a clash.
    """

    def configure(self, parser):
        parser.add_argument(
            "group",
            action="store",
            default="all",
            help="set of repositories to apply 'pull' to (\"all\" to pull all repositories)",
        )

        parser.add_argument(
            "-f",
            "--force",
            action="store_true",
            dest="force",
            help="force pull (potentially discards changes)",
        )

    def main(self, args):
        # Get logger
        logger = logging.getLogger("eufs_cli.pull")

        eufs_master = os.environ["EUFS_MASTER"]
        repo_info_path = os.path.join(eufs_master, ".eufscli", "repos.yaml")
        repo_ignore_path = os.path.join(eufs_master, ".eufscli", "eufsignore.yaml")

        # Read yaml file with eufs repository paths
        with open(repo_info_path, "r") as f:
            repo_info = yaml.load(f.read(), Loader=yaml.Loader)

        with open(repo_ignore_path, "r") as f:
            repo_ignore = yaml.load(f.read(), Loader=yaml.Loader)

        # read the eufs-master/src directories for groups and their repos
        group_info = {}  # dict{group name : [group repo]}
        groups_root_src = os.path.join(eufs_master, "src")
        if os.path.exists(groups_root_src):
            # read each group's repos
            for group_name in os.listdir(groups_root_src):
                group_dir = os.path.join(groups_root_src, group_name)
                group_repos = []
                for group_repo in os.listdir(group_dir):
                    group_repo_dir = os.path.join(groups_root_src, group_name, group_repo)
                    group_repos.append(group_repo_dir)
                group_info[group_name] = group_repos
        else:
            # even if path does not exist, all and single repos should work fine
            logger.info(Fore.RED + groups_root_src +
                        " does not exist. "
                        + "Relying on repos.yaml only and thus cannot view group repos. "
                        + Fore.RESET)

        # Pull all repositories
        if args.group == "all":
            logger.info(Fore.GREEN + "Pulling changes on all repos" + Fore.RESET)
            for (repo, repo_path) in repo_info.items():
                # ignore the external repositories specified in .eufscli/eufsignore.yaml
                if repo in repo_ignore:
                    logger.info(Fore.BLUE + f"Skipping external repo: {repo}" + Fore.RESET)
                    continue
                logger.info(Fore.BLUE + f"Pulling changes in {repo}" + Fore.RESET)
                self.checkout_repo(repo_path, args)
        # Pull every repository in group
        elif args.group in group_info:
            logger.info(Fore.GREEN + f"Pulling changes in group '{args.group}'" + Fore.RESET)
            for repo in group_info[args.group]:
                if repo in repo_ignore:
                    logger.info(Fore.BLUE + f"Skipping external repo: {repo}" + Fore.RESET)
                    continue
                logger.info(Fore.BLUE + f"Pulling changes in {repo}" + Fore.RESET)
                self.checkout_repo(repo, args)
        # Pull single repository; remember to update repos.yaml file using `eufs update`
        elif args.group in repo_info:
            logger.info(Fore.BLUE + f"Pulling changes in '{args.group}'" + Fore.RESET)
            self.checkout_repo(repo_info[args.group], args)
        # Invalid argument
        else:
            raise Exception("Invalid positional argument 'group'")

    def checkout_repo(self, repo_path, args):
        if args.force:
            Popen(["git", "-C", repo_path, "pull", "-f"]).wait()
        else:
            Popen(["git", "-C", repo_path, "pull"]).wait()
