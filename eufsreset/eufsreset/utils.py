import subprocess


def run_shell_command(cmd, verbose=False):
    try:
        popen = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE if not verbose else None,
            stderr=subprocess.PIPE if not verbose else None,
            text=True,
        )

        popen.wait()

        if popen.returncode != 0:
            return {"code": 1, "msg": popen.stderr.read()}
    except Exception as e:
        return {"code": 2, "msg": e}

    return {"code": 0, "msg": "success"}
