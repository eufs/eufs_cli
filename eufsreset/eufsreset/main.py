import logging
import os

import colorama

from eufscli import CommandExtension
from eufscli.utils.validation import input_yn

from .utils import run_shell_command

from git import Repo
import shutil


class EUFSReset(CommandExtension):
    """
    Resets all submodules.

    This command will call `git submodule update --recursive` in the directory
    pointed to by the environment variable EUFS_MASTER (we assume this is
    eufs-master). The effect of this is that all the submodules will get
    checked out to the commit pointed to by either eufs-master or their parent
    repository if it is deeper down the repository tree.
    """

    def configure(self, parser):
        parser.add_argument(
            "-v",
            "--verbose",
            action="store_true",
            dest="verbose",
            help="print output from shell commands",
        )
        parser.add_argument(
            "-f",
            "--force",
            action="store_true",
            dest="force",
            help="force reset (potentially discards changes)",
        )
        parser.add_argument(
            "--purge",
            action="store_true",
            dest="purge",
            help="remove all unused submodules",
        )

        self.repo = Repo(os.environ["EUFS_MASTER"])

    def main(self, args):
        # Get logger
        logger = logging.getLogger("eufs_cli.reset")

        # Ask for confirmation
        if not input_yn("Are you sure you want to reset the CLI?", logger):
            logger.info("Exiting...")
            return

        logger.info("Reset git submodules...")
        command = [
            "git",
            "-C",
            os.environ["EUFS_MASTER"],
            "submodule",
            "update",
            "--recursive",
        ]
        command += ["-f"] if args.force else []  # Add force flag if set
        result = run_shell_command(command, args.verbose)

        # remove unused submodules
        if args.purge:
            non_submodule_files = self.repo.git.ls_files(
                others=True, exclude_standard=True).split("\n")
            # filter out folders that are not git repos
            non_submodule_files = [file for file in non_submodule_files if os.path.isdir(
                os.path.join(os.environ["EUFS_MASTER"], file, ".git"))]

            if len(non_submodule_files) == 0:
                logger.info("No repositories to purge.")

            else:
                logger.info("Repositories to purge:")

                for file in non_submodule_files:
                    logger.info(f"{file}")

                if input_yn(
                    "Are you sure you want to delete these files?", logger
                ):
                    logger.info("Deleting files...")
                    for file in non_submodule_files:
                        file_path = os.path.join(os.environ["EUFS_MASTER"], file)
                        if os.path.isfile(file_path):
                            os.remove(file_path)
                        else:
                            shutil.rmtree(file_path, ignore_errors=True)

                        if args.verbose:
                            logger.info(f"Deleted {file}")

        if result["code"] == 0:
            logger.info(colorama.Fore.GREEN + "Done." + colorama.Fore.RESET)
        elif result["code"] == 1:
            logger.error(colorama.Fore.RED + "error:" + colorama.Fore.RESET)
            logger.error(result["msg"])
        else:
            logger.error(colorama.Fore.RED + "exception:" + colorama.Fore.RESET)
            logger.error(result["msg"])
