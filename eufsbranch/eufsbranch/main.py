import logging
import os
from subprocess import PIPE, Popen

import yaml
from colorama import Fore

from eufscli import CommandExtension


class EUFSBranch(CommandExtension):
    """
    Display the current branch on all submodules.

    Effectively runs `git branch` in each submodule to obtain the current
    branch in the said submodule. Some processing is done to obtain
    the current branch from the list of branches that may be displayed
    when running `git branch`. The `--show-current` flag was not used
    because the submodules whose HEAD is detached returned nothing.

    User can specify a group or repo name to view its branch only. Group
    names take precedence over repo names.
    """

    def configure(self, parser):
        parser.add_argument(
            "group",
            action="store",
            default="all",
            help="select a repo, a group or all to view the repos' branches"
        )

    def main(self, args):
        logger = logging.getLogger("eufs_cli.branch")
        logger.debug("Running eufs branch")

        eufs_master = os.environ["EUFS_MASTER"]
        # read the eufs-master/src directories for groups and their repos
        group_info = {}  # dict{group name : [group repo]}
        groups_root_src = os.path.join(eufs_master, "src")
        if os.path.exists(groups_root_src):
            # read each group's repos
            for group_name in os.listdir(groups_root_src):
                group_dir = os.path.join(groups_root_src, group_name)
                group_repos = []
                for group_repo in os.listdir(group_dir):
                    group_repo_dir = os.path.join(groups_root_src, group_name, group_repo)
                    group_repos.append(group_repo_dir)
                group_info[group_name] = group_repos
        else:
            # even if path does not exist, all and single repos should work fine
            logger.info(Fore.RED + groups_root_src +
                        " does not exist. "
                        + "Relying on repos.yaml only and thus cannot view group repos. "
                        + Fore.RESET)

        # Relying on the repos.yaml for a single layer of all repos list
        repo_info_path = os.path.join(eufs_master, ".eufscli", "repos.yaml")

        # Read yaml file with eufs repository paths
        with open(repo_info_path, "r") as f:
            repo_info = yaml.load(f.read(), Loader=yaml.Loader)

        # Sort contents of yaml file based on paths
        repo_info = dict(sorted(repo_info.items(), key=lambda item: item[1]))

        published_directories = set()

        def process_repo_path(path_elements):
            # display repo:branch based on file structure in eufs-master
            master_length = len(repo_info["eufs-master"].split("/"))
            path_length = len(path_elements)
            branch = self.view_branch(repo_path)
            for n in range(path_length - master_length + 1):
                element = path_elements[master_length + n - 1]
                if element not in published_directories:
                    if element != repo:
                        published_directories.add(element)
                        logger.info("  " * n + Fore.YELLOW + f"{element}")
                    else:
                        logger.info("  " * n + Fore.BLUE + f"{repo}: " + Fore.RESET + f"{branch}")

        if args.group == "all":
            logger.info(Fore.BLUE + "Viewing branches of all repos..." + Fore.RESET)
            for (repo, repo_path) in repo_info.items():
                path_elements = repo_path.split("/")
                process_repo_path(path_elements)
        elif args.group in group_info:  # single group name
            logger.info(Fore.BLUE + f"Viewing branches of {args.group} group" + Fore.RESET)
            published_directories.update(["eufs-master", "src"])
            for (repo, repo_path) in repo_info.items():
                path_elements = repo_path.split("/")
                if args.group in path_elements:
                    process_repo_path(path_elements)
        elif args.group in repo_info:  # single repo name
            logger.info(Fore.BLUE + f"Viewing branch of {args.group}" + Fore.RESET)
            branch = self.view_branch(repo_info[args.group])
            logger.info(Fore.BLUE + f"{args.group}: " + Fore.RESET + f"{branch}")
        # Invalid argument
        else:
            raise Exception(f"Invalid positional argument 'group': {args.group}")

    def view_branch(self, repo_path: str) -> str:
        # Get branch info
        proc = Popen(
            f"git -C {repo_path} --no-pager branch",
            shell=True,
            stdout=PIPE,
        )
        # Parse output
        output = proc.stdout.read().decode()
        branch = [x.replace("*", "").strip()
                  for x in output.split("\n") if "* " in x]
        return branch[0]
