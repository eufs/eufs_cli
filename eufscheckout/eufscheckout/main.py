import logging
import os
from subprocess import Popen

import yaml
from colorama import Fore

from eufscli import CommandExtension


class EUFSCheckout(CommandExtension):
    """
    Checkout the same branch in all submodules.

    Basically just runs `git checkout <branch_name>` in every submodule.
    If a submodule does not contain the branch <branch_name>, then
    an error message is printed to the screen and we move on to the next
    submodule.

    The user can decide which submodules to apply the `checkout` operation
    to. This is specified in the `group` positional argument. This argument
    takes either a repository name, in which case only that repository
    will get the `checkout` applied to it, or a repository group. A
    repository group is an EUFS sub team, e.g. Infrastructure. These corresponds
    to the directories under $EUFS_MASTER/src/.

    Group names take precedence over repo names in case there is a clash.
    """

    def configure(self, parser):
        parser.add_argument(
            "group",
            action="store",
            default="all",
            help="set of repositories to apply 'checkout'  to",
        )
        parser.add_argument("branch", action="store", help="branch to checkout")
        parser.add_argument(
            "-f",
            "--force",
            action="store_true",
            dest="force",
            help="force checkout (potentially discards changes)",
        )

    def main(self, args):
        # Get logger
        logger = logging.getLogger("eufs_cli.checkout")

        eufs_master = os.environ["EUFS_MASTER"]
        repo_info_path = os.path.join(eufs_master, ".eufscli", "repos.yaml")

        # Read yaml file with eufs repository paths
        with open(repo_info_path, "r") as f:
            repo_info = yaml.load(f.read(), Loader=yaml.Loader)

        # read the eufs-master/src directories for groups and their repos
        group_info = {}  # dict{group name : [group repo]}
        groups_root_src = os.path.join(eufs_master, "src")
        if os.path.exists(groups_root_src):
            # read each group's repos
            for group_name in os.listdir(groups_root_src):
                group_dir = os.path.join(groups_root_src, group_name)
                group_repos = []
                for group_repo in os.listdir(group_dir):
                    group_repo_dir = os.path.join(groups_root_src, group_name, group_repo)
                    group_repos.append(group_repo_dir)
                group_info[group_name] = group_repos
        else:
            # even if path does not exist, all and single repos should work fine
            logger.info(Fore.RED + groups_root_src +
                        " does not exist. "
                        + "Relying on repos.yaml only and thus cannot view group repos. "
                        + Fore.RESET)

        # Checkout all repositories
        if args.group == "all":
            logger.info(Fore.GREEN + "Checking out all repos..." + Fore.RESET)
            for (repo, repo_path) in repo_info.items():
                logger.info(Fore.BLUE + f"Checking out {args.branch} in {repo}..." + Fore.RESET)
                self.checkout_repo(repo_path, args)
        # Checkout every repository in group
        elif args.group in group_info:
            logger.info(
                Fore.GREEN + f"Checking out all repos in group '{args.group}''..." + Fore.RESET
            )
            for repo in group_info[args.group]:
                logger.info(Fore.BLUE + f"Checking out {args.branch} in {repo}..." + Fore.RESET)
                self.checkout_repo(repo, args)
        # Checkout single repository
        elif args.group in repo_info:
            logger.info(Fore.BLUE + f"Checking out {args.branch} in {args.group}..." + Fore.RESET)
            self.checkout_repo(repo_info[args.group], args)
        # Invalid argument
        else:
            raise Exception("Invalid positional argument 'group'")

    def checkout_repo(self, repo_path, args):
        if args.force:
            Popen(["git", "-C", repo_path, "checkout", "-f", args.branch]).wait()
        else:
            Popen(["git", "-C", repo_path, "checkout", args.branch]).wait()
