import logging
import os
from glob import glob
from subprocess import PIPE, Popen

import yaml

from eufscli import CommandExtension


class EUFSUpdate(CommandExtension):
    """
    Update eufscli.

    Make sure that eufscli is on the most recent changes. It will also recreate
    the repos.yaml used by commands such a eufsbranch and eufscheckout.
    """

    def configure(self, parser):
        parser.add_argument(
            "packages",
            nargs="*",
            default=[],
            help="packages to update e.g install (eufsinstall also works)",
        )

        parser.add_argument(
            "-t",
            "--tree-only",
            action="store_true",
            dest="tree_only",
            help="Only update the repo tree",
        )

    def main(self, args):
        # Update eufscli to the latest changes
        if not args.tree_only:
            self.update(args)

        # Create a file with every submodule and its path
        self.build_repo_tree()

    def update(self, args):
        # Get logger
        logger = logging.getLogger("eufs_cli.update")

        # Update eufscli
        logger.info("Pulling latest changes")
        result = Popen("cd $EUFS_CLI_HOME && git pull", shell=True).wait()
        if result == 0:
            # Get paths to all packages in $EUFS_CLI_HOME
            packages = glob(f"{os.environ['EUFS_CLI_HOME']}/eufs*")

            # Get list of packages to install
            packages_to_install = [] if args.packages else packages

            if args.packages:
                pkg_names = {path.split("/")[-1]: path for path in packages}
                for name in args.packages:
                    tmp = [pkg_names[pkg_name] for pkg_name in pkg_names.keys() if name in pkg_name]
                    if len(tmp) > 2:
                        logger.warn(
                            f"Installing multiple packages with name {name}: {' '.join(tmp)}"
                        )
                    elif len(tmp) == 0:
                        logger.warn(f"Found no packages with name {name}: {' '.join(tmp)}")

                    packages_to_install.append(*tmp)

            # Install packages
            for package_path in packages_to_install:
                logger.info(f"Installing {package_path}")
                Popen(f"pip3 install {package_path}", shell=True).wait()

    def build_repo_tree(self):
        # Get logger
        logger = logging.getLogger("eufs_cli.update")
        logger.info("Building repo tree...")

        # Get all submodules
        proc = Popen(
            [
                "git",
                "-C",
                os.environ["EUFS_MASTER"],
                "submodule",
                "status",
                "--recursive",
            ],
            stdout=PIPE,
        )
        output = proc.stdout.read().decode()

        # Extract path of submodules
        submodules_path = [x.strip().split(" ")[1] for x in output.split("\n") if x != ""]

        # Get list of [submodule name, full_submodule_path]
        submodules = [
            [x.split("/")[-1], os.path.join(os.environ["EUFS_MASTER"], x)] for x in submodules_path
        ]

        # Convert to dict so we can write to yaml file
        submodule_dict = {"eufs-master": os.environ["EUFS_MASTER"]}
        for submodule in submodules:
            submodule_dict[submodule[0]] = submodule[1]

        # Write submodules to file in .eufscli
        submodule_tree_path = os.path.join(os.environ["EUFS_MASTER"], ".eufscli", "repos.yaml")
        with open(submodule_tree_path, "w") as f:
            f.write(yaml.dump(submodule_dict))
