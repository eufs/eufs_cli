#!/usr/bin/env bash

# Install rosdeps
rosdep install --from-paths ~/eufs-master/src --ignore-src -r -y
