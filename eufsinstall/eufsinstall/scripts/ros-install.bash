#!/usr/bin/env bash

# args:
#   $1: password for sudo

# Authorize ROS GPG key
echo $1 | sudo -S apt update
echo $1 | sudo -S apt install -y curl gnupg2 lsb-release
echo $1 | sudo -S curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg

# Add ROS repository to sources list
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

# Install ROS
echo $1 | sudo -S apt update
echo $1 | sudo -S apt install -y ros-foxy-desktop

# Install rosdep
echo $1 | sudo -S apt install -y python3-rosdep
echo $1 | sudo -S rosdep init
rosdep -y update

# Install argcomplete
echo $1 | sudo -S apt install -y python3-argcomplete

# Install colcon
echo $1 | sudo -S apt install -y python3-colcon-common-extensions

# Install other dependencies
echo $1 | sudo -S apt install -y ros-foxy-gazebo-ros-pkgs
echo $1 | sudo -S apt install -y ros-foxy-gazebo-ros
echo $1 | sudo -S apt install -y ros-foxy-xacro
echo $1 | sudo -S apt install -y ros-foxy-joint-state-publisher
echo $1 | sudo -S apt install -y ros-foxy-camera-info-manager

echo "Done."

exit 0
