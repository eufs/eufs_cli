#!/usr/bin/env bash

# args:
#    $1: password for sudo

#######################
# Cuda Pre-requisites #
#######################
#
# See https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#pre-installation-actions
#


# Check to make sure gcc >= 9.0.0
currentver="$(gcc -dumpversion)"
requiredver="9.0.0"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" = "$requiredver" ]; then
        echo "gcc >= $requiredver, continuing..."
else
        echo "gcc version less than ${requiredver}, stop."
	exit 1
fi

# Install kernel headers and development packages for current kernel
echo $1 | sudo -S apt-get install linux-headers-$(uname -r)

#####################
# Cuda Installation #
#####################
#
# See https://developer.nvidia.com/cuda-11.2.0-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=2004&target_type=deblocal
#

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
echo $1 | sudo -S mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.2.0/local_installers/cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb
echo $1 | sudo -S dpkg -i cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb
echo $1 | sudo -S apt-key add /var/cuda-repo-ubuntu2004-11-2-local/7fa2af80.pub
echo $1 | sudo -S apt-get update
echo $1 | sudo -S apt-get -y install cuda

##########################
# Cuda Post-installation #
##########################
#
# See https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#post-installation-actions
#

if test -f $HOME/.bashrc; then
    # Must use single quotes or the expression will be evaluated by echo
    echo 'export PATH=/usr/local/cuda-11.4/bin${PATH:+:${PATH}}' >> $HOME/.bashrc
fi

if test -f $HOME/.zshrc; then
    # Must use single quotes or the expression will be evaluated by echo
    echo 'export PATH=/usr/local/cuda-11.4/bin${PATH:+:${PATH}}' >> $HOME/.zshrc
fi
