#!/usr/bin/env bash

# args:
#     $1: password for sudo

echo $1 | sudo -S apt update
echo $1 | sudo -S apt -y install openssh-server
echo $1 | sudo -S ufw allow ssh
