#!/usr/bin/env bash

# Clone eufs-master
git -C ~ clone git@gitlab.com:eufs/eufs-master.git

# Initialize submodules
git -C ~/eufs-master submodule update --init --recursive
