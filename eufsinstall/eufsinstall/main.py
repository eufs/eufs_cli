import subprocess
import sys
from getpass import getpass

import colorama
from pkg_resources import resource_filename

from eufscli import CommandExtension

from .utils import run_shell_command


class EUFSInstall(CommandExtension):
    """Installs ROS, clones eufs-master & installs dependencies."""

    def configure(self, parser):
        parser.add_argument(
            "-v",
            "--verbose",
            action="store_true",
            dest="verbose",
            help="print output from shell commands",
        )

    def get_pwd(self):
        # Get password for sudo operations
        pwd = getpass("Enter password: ")
        while not self.test_pwd(pwd):
            pwd = getpass("\nEnter password: ")

        return pwd

    def test_pwd(self, pwd):
        test = subprocess.Popen(
            ["sudo", "-vkS"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        test.communicate(input=bytes(pwd, "utf-8"))
        test.wait()

        return test.returncode == 0

    def install_ros(self, pwd, verbose):
        print(
            colorama.Fore.GREEN,
            "Installing ROS...",
            colorama.Fore.RESET,
            flush=True,
        )

        install_path = resource_filename(__name__, "scripts/ros-install.bash")
        result = run_shell_command(["bash", install_path, pwd], verbose)

        if result["code"] != 0:
            sys.exit(result["msg"])

    def clone_eufs(self, verbose):
        print(
            colorama.Fore.GREEN,
            "Cloning eufs-master...",
            colorama.Fore.RESET,
            flush=True,
        )

        clone_eufs_path = resource_filename(__name__, "scripts/clone-eufs.bash")
        result = run_shell_command(["bash", clone_eufs_path], verbose)

        if result["code"] != 0:
            sys.exit(result["msg"])

    def rosdep_install(self, verbose):
        print(
            colorama.Fore.GREEN,
            "Installing rosdep deps...",
            colorama.Fore.RESET,
            flush=True,
        )

        rosdep_install_path = resource_filename(__name__, "scripts/rosdep.bash")
        result = run_shell_command(["bash", rosdep_install_path], verbose)

        if result["code"] != 0:
            sys.exit(result["msg"])

    def main(self, args):
        # Get password for sudo operations
        pwd = self.get_pwd()

        self.install_ros(pwd, args.verbose)
        self.clone_eufs(args.verbose)
        self.rosdep_install(args.verbose)
